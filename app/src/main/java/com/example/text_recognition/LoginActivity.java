package com.example.text_recognition;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.text_recognition.Module.CheckConnect;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    Window window;
    Button btnLogin;
    EditText loginEmail, loginPass;
    TextView txtCreate, txtForgot;
    static boolean count;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(Build.VERSION.SDK_INT>=23)
        {
            window=this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorWhile));
        }
        count = false;
        mAuth = FirebaseAuth.getInstance();
        Connect();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
                if(mFirebaseUser!= null)
                {
                    if(CheckConnect.isNetworkAvailable(getApplication()))
                    {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        CheckConnect.ShowToast_Short(getApplication(), "Bạn chưa kết nối mạng.");
                    }
                }
            }
        };

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
                pd.setMessage("Login in...");
                if(CheckConnect.isNetworkAvailable(getApplication()))
                {
                    String email = loginEmail.getText().toString().trim();
                    String password = loginPass.getText().toString().trim();
                    if(email.isEmpty())
                    {
                        loginEmail.setError("Vui lòng nhập email");
                        loginEmail.requestFocus();
                    }
                    else if(password.isEmpty())
                    {
                        loginPass.setError("Vui lòng nhập mật khẩu");
                        loginPass.requestFocus();
                    }
                    else if(email.isEmpty() && password.isEmpty())
                    {
                        Toast.makeText(LoginActivity.this, "Bạn chưa nhập email và mật khẩu !", Toast.LENGTH_SHORT).show();
                    }
                    else if(!email.isEmpty() && !password.isEmpty())
                    {
                        pd.show();
                        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                pd.dismiss();
                                if(!task.isSuccessful())
                                {
                                    Toast.makeText(LoginActivity.this, "Đăng nhập không thành công", Toast.LENGTH_SHORT). show();
                                }
                                else
                                {
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(LoginActivity.this, "Lỗi!!!", Toast.LENGTH_SHORT). show();
                    }
                }
                else
                {
                    CheckConnect.ShowToast_Short(getApplication(), "Bạn chưa kết nối mạng.");
                }

            }
        });

        txtCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CheckConnect.isNetworkAvailable(getApplication()))
                {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    startActivity(intent);
                }
                else
                {
                    CheckConnect.ShowToast_Short(getApplication(), "Bạn chưa kết nối mạng.");
                }

            }
        });
        txtForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showForgotPasswordDialog();
            }
        });
    }

    private void showForgotPasswordDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Forgot Password");

        LinearLayout linearLayout = new LinearLayout(this);

        final EditText emailForgot = new EditText(this);
        emailForgot.setHint("Email");
        emailForgot.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        linearLayout.addView(emailForgot);
        linearLayout.setPadding(10,10,10,10);

        builder.setView(linearLayout);
        builder.setPositiveButton("Recover", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = emailForgot.getText().toString().trim();
                Recovery(email);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

    private void Recovery(String email) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Sending email...");
        pd.show();
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                pd.dismiss();
                if(task.isSuccessful())
                {
                    Toast.makeText(LoginActivity.this, "Email sent", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Failed...", Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Connect() {
        txtCreate=findViewById(R.id.loginCreate);
        txtForgot=findViewById(R.id.loginForgot);

        btnLogin=findViewById(R.id.btnLogin);

        loginEmail=findViewById(R.id.loginEmail);
        loginPass=findViewById(R.id.loginPass);
    }

    @Override
    protected void onResume() {
        count = false;
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        android.os.Process.killProcess(android.os.Process.myPid());
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // Do Here what ever you want do on back press;
        if(!count){
            count = true;
            Toast.makeText(this, "Chạm lần nữa để thoát", Toast.LENGTH_SHORT).show();
        } else{
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);
    }
}
