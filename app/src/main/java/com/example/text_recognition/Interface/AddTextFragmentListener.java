package com.example.text_recognition.Interface;

public interface AddTextFragmentListener {
    void onAddTextButtonClick(String text, int color);
}
